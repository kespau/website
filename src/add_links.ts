let tools = [
    document.getElementById("typescript"),
    document.getElementById("rust"),
    document.getElementById("csharp"),
    document.getElementById("python"),
    document.getElementById("haxe"),
  ];
  let tool_links = [
    "https://www.typescriptlang.org/",
    "https://www.rust-lang.org/",
    "https://learn.microsoft.com/en-us/dotnet/csharp/",
    "https://www.python.org/",
    "https://haxe.org/",
  ];
  
  for (let tool of tools) {
      tool.addEventListener('click', (event) => {
          window.open(tool_links[tools.indexOf(tool)]);
      });
  }
  